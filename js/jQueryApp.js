/* jQueryApp.js
 */

/* Based upon
 * Start Bootstrap - Stylish Portfolio v5.0.0
 * (https://startbootstrap.com/template-overviews/stylish-portfolio)
 * Copyright 2013-2018 Start Bootstrap
 * Licensed under MIT
 * (https://github.com/BlackrockDigital/startbootstrap-stylish-portfolio/blob/master/LICENSE)
 */

console.log("jQueryApp entered...");

(function($) {
    "use strict"; // Start of use strict
    // Closes the sidebar menu
    $(".menu-toggle").click((e) => {
        e.preventDefault()
        $("#sidebar-wrapper").toggleClass("active")
        $(".menu-toggle > .fa-bars, .menu-toggle > .fa-times").toggleClass("fa-bars fa-times")
        $(this).toggleClass("active")
    })

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(() => {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            let target = $(this.hash)
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']')
            if (target.length) {
                const speed = 500 // ms
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, speed, "easeInOutExpo")
                return false
            }
        }
    })

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function() {
        $('.navbar-collapse').collapse('hide')
    })

    // Closes responsive sidebar menu when a scroll trigger link is clicked
    $('#sidebar-wrapper .js-scroll-trigger').click(() => {
        $("#sidebar-wrapper").removeClass("active")
        $(".menu-toggle").removeClass("active")
        $(".menu-toggle > .fa-bars, .menu-toggle > .fa-times").toggleClass("fa-bars fa-times")
    })

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#sideNav'
    })

    // Scroll to top button appear
    $(document).scroll(() => {
        var scrollDistance = $(this).scrollTop()
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn()
        } else {
            $('.scroll-to-top').fadeOut()
        }
    })

}(jQuery)) // End of use strict

// Disable Google Maps scrolling
// See http://stackoverflow.com/a/25904582/1607849
// Disable scroll zooming and bind back the click event
const onMapMouseleaveHandler = (event) => {
    this.on('click', onMapClickHandler)
    this.off('mouseleave', onMapMouseleaveHandler)
    this.find('iframe').css("pointer-events", "none")
}

const onMapClickHandler = (event) => {
    // Disable the click handler until the user leaves the map area
    this.off('click', onMapClickHandler)
    // Enable scrolling zoom
    this.find('iframe').css("pointer-events", "auto")
    // Handle the mouse leave event
    this.on('mouseleave', onMapMouseleaveHandler)
}

// Enable map zooming with mouse scroll when the user clicks the map
$('.map').on('click', onMapClickHandler)

console.log("...leaving jQueryApp.")