@echo off
:: depends on node-sass
setlocal
(set srcdir=scss)
(set in=website.scss)
(set outdir=css)
(set out=website.css)

sass -I %srcdir% %srcdir%\%in% %outdir%\%out% --update --watch

endlocal
